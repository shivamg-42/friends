# frozen_string_literal: true

# this is a controller to get the main app
class HomeController < ApplicationController
  def index; end

  def about; end
end
